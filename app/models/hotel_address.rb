# 每一次判断需要使用一个实例, 实例非线程安全
class HotelAddress
  include WeixinRailsMiddleware::ReplyWeixinMessageHelper
  include Concerns::Bitdo
  include HTTParty

  AK = 'EDa3b6c07b4ab7485388e60f2813b643'
  # 不确定这个坐标是否准确
  HOTEL_NAME = '长沙鑫远白天鹅酒店'

  def initialize(keyword = '')
    @keyword = keyword
  end

  # 第一步询问如何去
  def is_ask?
    @keyword.index('地址') == 0 || @keyword.index('酒店') == 0 || @keyword.index('怎么取') == 0
  end

  def reply_ask
    "请将微信的 '位置' 发给我们, 我们来帮你计算导航"
  end

  # 返回可用的文字版导航(数组)
  def hotel_direction(x, y)
    geoconv(x, y)
    result = direction(@geo['x'], @geo['y'])
    guides = []
    result['routes'].each_with_index do |route, index|
      steps = "****** 线路#{index + 1} ******\n"
      route['scheme'][0]['steps'].each { |step| steps << (step[0]['stepInstruction'] + "\n") }
      #guides << steps.gsub('<font color="#313233">', ' [').gsub('</font>', '] ')
      guides << clear_font_tag(steps)
    end
    guides
  end

  # 返回可用的文档导航选择
  def hotel_direction_articles(x, y)
    [generate_article("去[长沙鑫远白天鹅大酒店]可选择下列途径哦~", hotel_direction(x, y).join("\n\n"), '', dwz(uri_api))]
  end

  def uri_api
    %Q(http://api.map.baidu.com/direction?origin=latlng:#{@geo['x']},#{@geo['y']}|name:当前地址&destination=#{HOTEL_NAME}&mode=transit&region=长沙&output=html&src=Wyatt_love_Holly|Wyatt_love_Holly)
  end

  def clear_font_tag(text)
    mapping = {
        '<font color="#313233">' => ' [',
        '<font color="#7a7c80">' => ' [',
        '</font>' => '] ',
        '<b>' => '',
        '</b>' => ''
    }
    mapping.each { |k, v| text.gsub!(k, v) }
    text
  end

  def geoconv(x, y)
    @geo ||= MultiJson.load(self.class.get("http://api.map.baidu.com/geoconv/v1/?coords=#{x},#{y}&from=3&ak=#{AK}").body)['result'][0]
  end

  def direction(x, y)
    # 酒店在百度上的坐标
    # mode=transit&origin=#{x},#{y}&destination=#{hotel[:x]},#{hotel[:y]}&region=长沙&output=json&ak=#{AK}
    options = {mode: 'transit', origin: "#{x},#{y}", destination: "#{HOTEL_NAME}", region: '长沙', ak: AK, output: 'json'}
    MultiJson.load(self.class.get("http://api.map.baidu.com/direction/v1", query: options).body)['result']
  end

end