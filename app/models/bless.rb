class Bless
  include WeixinRailsMiddleware::ReplyWeixinMessageHelper

  class << self
    def help
      lines = [
          "Section 1:",
          "请参照以下简要消息指南哦~",
          "1.婚宴时间&地址, 回复\"喜\"",
          "2.想看新婚靓照吗?回复\"结\"",
          "3.想知道偶们第一次约会的地址吗?回复\"良\"",
          "4.偶们的纪念日, 回复\"缘\"",
          "5.新郎的中文名, 回复\"白\"",
          "6.新娘的中文名, 回复\"首\"",
          "7.偶们最经常的娱乐方式, 回复\"偕\"",
          "8.偶们的生日, 回复\"老\"",
          "",
          "Section 2:",
          "Wyatt&Holly非常感谢大家的关注, 2014.9.20(就爱你)是偶们结婚的大喜日子, 您的祝福和光临将是我们最大的荣耀\ue425",
          "",
          "祝福语收集:",
          "回复如 \"祝Wyatt&Holly, 永结同心, 百年好合\" 嘿嘿~~",
          "",
          "Section 3:",
          "发送你的位置给偶啦, 偶将告诉你怎么到达目的地哦\ue032"
      ]
      %Q{亲耐的盆友, 你的消息偶们已经收到了啦 \ue428\n#{signature}\n#{lines.join("\n")}}
    end

    def signature
      "               Wyatt \ue022 Holly\n"
    end
  end

  def initialize(keyword = '', msg = nil)
    @keyword = keyword
    @msg = msg
  end

  def is_bless?
    @keyword.start_with?('祝')
  end

  def is_replay?
    if @keyword.nil?
      false
    else
      %w(喜 结 良 缘 白 首 偕 老 "喜" "结" "良" "缘" "白" "首" "偕" "老").include?(@keyword.strip)
    end
  end

  def record_bless(msg_type: 'text')
    if record(msg_type: msg_type, is_bless: true)
      "谢谢您的祝福\ue414\n#{self.class.signature}"
    else
      Rails.logger.warn("#{msg.errors.full_messages}")
      "因为系统特殊 Wyatt & Holly 将您的信息记录在云端啦"
    end
  end


  def record_text(msg_type: 'text')
    record(msg_type: msg_type, is_bless: false)
    self.class.help
  end

  def record_reply(msg_type: 'text')
    record(msg_type: msg_type, is_bless: false)
    if %w(喜 "喜").include?(@keyword)
      {type: 'text', body: "2014 年 9 月 20 日 上午\ue02f, 鑫远白天鹅酒店 3 楼 春华厅"}
    elsif %w(结 "结").include?(@keyword)
      {type: 'img', body: 'main.jpg'}
    elsif %w(良 "良").include?(@keyword)
      {type: 'text', body: "Wyatt 觉得很浪漫的很浪漫的地方,岳麓山上看枫叶,其实难忘在\"我们走了一条不寻常的路下山, 结果不知道到了虾米地方了,迷路啦~~\" \ue40d"}
    elsif %w(缘 "缘").include?(@keyword)
      {type: 'text', body: "4 月 8 日, 2014 年的这一天也是我们登记结婚的好日子哦~~"}
    elsif %w(白 "白").include?(@keyword)
      {type: 'text', body: "盘巍, 每次报出偶的名字时,总会被人以羡慕且惊叹的眼神注视着半秒钟, 难道就是因为我的姓氏出自上古传说中开天辟地的盘古氏 \ue415"}
    elsif %w(首 "首").include?(@keyword)
      {type: 'text', body: "赵娴, \"缘来是你\" 有机会跟大家长篇大论解释一番 \ue056"}
    elsif %w(偕 "偕").include?(@keyword)
      {type: 'text', body: "看电影, 这是不是大多数盆友的约会方式呢? 应该不是的,对不对? 所以说 Wyatt 我的浪漫细胞还是有待增加的~~"}
    elsif %w(老 "老").include?(@keyword)
      {type: 'text', body: "Wyatt:616, Holly:525; 亲耐的们, 千万别误会哦, 偶们生日的时候不用送礼物的, 哈哈~~只是想说这样的生日是不是很有意思, 嘿嘿~~"}
    else
      false
    end
  end


  def record(msg_type: 'text', is_bless: true)
    msg = Msg.new(message: @keyword, msg_type: msg_type, is_bless: is_bless)
    msg.user_id = @msg.FromUserName if @msg
    msg.save
  end

end