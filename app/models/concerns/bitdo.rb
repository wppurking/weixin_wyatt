# Bitdo 短地址
module Concerns
  module Bitdo
    def dwz(url)
      hash = MultiJson.load(HTTParty.post('http://bit.do/mod_perl/url-shortener.pl', body: {action: 'shorten', url: url}).body)['url_hash']
      "http://bit.do/#{hash}"
    end
  end
end
