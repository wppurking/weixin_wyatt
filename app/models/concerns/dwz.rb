# 百度短地址
module Concerns
  module Dwz
    def dwz(url)
      MultiJson.load(HTTParty.post('http://www.dwz.cn/create.php', body: {url: url}))['tinyurl']
    end
  end
end
