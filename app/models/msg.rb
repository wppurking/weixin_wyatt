class Msg < ActiveRecord::Base

  validates :msg_type, inclusion: {in: %w(text image)}
  validates :msg_type, :message, presence: true

end
