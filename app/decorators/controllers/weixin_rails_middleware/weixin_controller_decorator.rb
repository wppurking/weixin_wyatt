# encoding: utf-8
# 1, @weixin_message: 获取微信所有参数.
# 2, @weixin_public_account: 如果配置了public_account_class选项,则会返回当前实例,否则返回nil.
# 3, @keyword: 目前微信只有这三种情况存在关键字: 文本消息, 事件推送, 接收语音识别结果
WeixinRailsMiddleware::WeixinController.class_eval do
  include Rails.application.routes.url_helpers

  def reply
    render xml: send("response_#{@weixin_message.MsgType}_message", {})
  end

  private

  def response_text_message(options={})
    hotel = HotelAddress.new(@keyword)
    bless = Bless.new(@keyword, @weixin_message)

    if bless.is_replay?
      reply = bless.record_reply
      if reply[:type] == 'img'
        # 图片尺寸使用 1240 * 800
        # http://brutaldesign.github.io/swipebox/
        article = generate_article('来看看偶们的美美照片吧', '', 'http://wyatt.qiniudn.com/main.jpg', pictures_url)
        reply_news_message([article])
      else
        reply_text_message(reply[:body])
      end
    elsif bless.is_bless?
      reply_text_message(bless.record_bless)
    elsif hotel.is_ask?
      reply_text_message(hotel.reply_ask)
    elsif  @keyword.downcase.include?('amazon')
      article = generate_article('EasyAcc EasyAcc 10000mAh Brilliant Ultra Slim Dual USB (2.1A / 1.5A Output) Portable Power Bank External Battery Charger (PB10000C)',
                                 %q(EasyAcc Make it easy EasyAcc PowerBank - Make it easy to keep running all the time Why choose EasyAcc PB10000C ? Aggressive Appearance A unique look that will let you stand out from the crowd. Powerful Dual USB Output Capability With 10000mAh at your fingertips, there's no need to worry about your battery running out. Talk to your friends, play games, or surf the internet for as long as you want....),
                                 'http://ecx.images-amazon.com/images/I/51m%2BFoL7CLL._SX522_.jpg',
                                 'http://www.amazon.com/dp/B00H9BEC8E')
      reply_news_message([article])
    else
      reply_text_message(bless.record_text)
    end
  end

  # <Location_X>23.134521</Location_X>
  # <Location_Y>113.358803</Location_Y>
  # <Scale>20</Scale>
  # <Label><![CDATA[位置信息]]></Label>
  # 只要客户提请地址, 则根据地址计算到酒店的路径
  def response_location_message(options={})
    @lx = @weixin_message.Location_X
    @ly = @weixin_message.Location_Y
    @scale = @weixin_message.Scale
    @label = @weixin_message.Label
    reply_news_message(HotelAddress.new.hotel_direction_articles(@lx, @ly))
  end

  # <PicUrl><![CDATA[this is a url]]></PicUrl>
  # <MediaId><![CDATA[media_id]]></MediaId>
  def response_image_message(options={})
    @media_id = @weixin_message.MediaId # 可以调用多媒体文件下载接口拉取数据。
    @pic_url = @weixin_message.PicUrl # 也可以直接通过此链接下载图片, 建议使用carrierwave.
    puts "Media_id: #{@media_id}"
    puts "Pic URL: #{@pic_url}"
    # reply_image_message(generate_image(@media_id))
    # --------------
    # 1. 需要准备一些列的合适大小的照片
    # 2. 随机从这些照片中选择出一张照片使用
    # 3. 生成一张用于展示图片的页面. 主要用于控制图片在手机上的大小
    articles = generate_article('收到您的照片, 我也来回你一张哈~', '', 'http://wyattholly.ngrok.com/assets/p4.jpg', 'http://wyattholly.ngrok.com/assets/p4.jpg')
    reply_news_message([articles])
  end

  # <Title><![CDATA[公众平台官网链接]]></Title>
  # <Description><![CDATA[公众平台官网链接]]></Description>
  # <Url><![CDATA[url]]></Url>
  def response_link_message(options={})
    @title = @weixin_message.Title
    @desc = @weixin_message.Description
    @url = @weixin_message.Url
    reply_text_message("回复链接信息")
  end

  # <MediaId><![CDATA[media_id]]></MediaId>
  # <Format><![CDATA[Format]]></Format>
  def response_voice_message(options={})
    reply_text_message("您的消息收到./:8*, 可是我们不支持语音呀\ue107")
  end

  # <MediaId><![CDATA[media_id]]></MediaId>
  # <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
  def response_video_message(options={})
    @media_id = @weixin_message.MediaId # 可以调用多媒体文件下载接口拉取数据。
    # 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
    @thumb_media_id = @weixin_message.ThumbMediaId
    reply_text_message("回复视频信息")
  end

  def response_event_message(options={})
    event_type = @weixin_message.Event
    send("handle_#{event_type.downcase}_event")
  end

  private

  # 关注公众账号
  def handle_subscribe_event
    if @keyword.present?
      # 扫描带参数二维码事件: 1. 用户未关注时，进行关注后的事件推送
      return reply_text_message("扫描带参数二维码事件: 1. 用户未关注时，进行关注后的事件推送, keyword: #{@keyword}")
    end
    reply_text_message("Wyatt&Holly非常感谢大家的关注, 2014.9.20(就爱你)是偶们结婚的大喜日子, 您的祝福和光临将是我们最大的荣耀.\n\n#{Bless.help}")
  end

  # 取消关注
  def handle_unsubscribe_event
    Rails.logger.info("取消关注")
  end

  # 扫描带参数二维码事件: 2. 用户已关注时的事件推送
  def handle_scan_event
    reply_text_message("扫描带参数二维码事件: 2. 用户已关注时的事件推送, keyword: #{@keyword}")
  end

  def handle_location_event # 上报地理位置事件
    @lat = @weixin_message.Latitude
    @lgt = @weixin_message.Longitude
    @precision = @weixin_message.Precision
    reply_text_message("Your Location: #{@lat}, #{@lgt}, #{@precision}")
  end

  # 点击菜单拉取消息时的事件推送
  def handle_click_event
    reply_text_message("你点击了: #{@keyword}")
  end

  # 点击菜单跳转链接时的事件推送
  def handle_view_event
    Rails.logger.info("你点击了: #{@keyword}")
  end

  # 帮助文档: https://github.com/lanrion/weixin_authorize/issues/22

  # 由于群发任务提交后，群发任务可能在一定时间后才完成，因此，群发接口调用时，仅会给出群发任务是否提交成功的提示，若群发任务提交成功，则在群发任务结束时，会向开发者在公众平台填写的开发者URL（callback URL）推送事件。

  # 推送的XML结构如下（发送成功时）：

  # <xml>
  # <ToUserName><![CDATA[gh_3e8adccde292]]></ToUserName>
  # <FromUserName><![CDATA[oR5Gjjl_eiZoUpGozMo7dbBJ362A]]></FromUserName>
  # <CreateTime>1394524295</CreateTime>
  # <MsgType><![CDATA[event]]></MsgType>
  # <Event><![CDATA[MASSSENDJOBFINISH]]></Event>
  # <MsgID>1988</MsgID>
  # <Status><![CDATA[sendsuccess]]></Status>
  # <TotalCount>100</TotalCount>
  # <FilterCount>80</FilterCount>
  # <SentCount>75</SentCount>
  # <ErrorCount>5</ErrorCount>
  # </xml>
  def handle_masssendjobfinish_event
    Rails.logger.info("回调事件处理")
  end

end
