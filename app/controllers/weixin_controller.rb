class WeixinController < ApplicationController
  # ------- 不使用 Gem 自己实现与微信接口
  skip_before_filter :verify_authenticity_token

  before_filter :valid_message

  def index
    render text: params[:echostr]
  end

  def create
    puts "======================#{request.raw_post}"
    puts params[:xml]
  end

  private
  def valid_message
    array = [Rails.configuration.weixin_token, params[:timestamp], params[:nonce]].sort
    render :text => "Forbidden", :status => 403 if params[:signature] != Digest::SHA1.hexdigest(array.join)
  end


end
