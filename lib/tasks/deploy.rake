require 'sshkit'
require 'sshkit/dsl'

namespace :deploy do
  #SSHKit.config.output_verbosity = :info
  SSHKit.config.output_verbosity = :debug

  HOST = 'root@wyatt-love-holly.com'

  task :restart do
    on(HOST) do |host|
      execute 'cd ~/weixin_wyatt/;mkdir -p tmp/pids;git fetch;git reset --hard origin/master'
      execute 'cd ~/weixin_wyatt/;'
    end
  end
end