require 'spec_helper'


describe HotelAddress do

  describe '#hotel_direction' do
    let(:result) do
      file = File.open('spec/models/direction.txt', 'r').read
      MultiJson.load(file)['result']
    end

    it '应该为 Hash' do
      expect(result.class).to eq Hash
    end

    it '测试' do
      result['routes'][0]['scheme'][0]['steps'].each do |step|
        puts step[0]['stepInstruction']
      end
    end
  end
end