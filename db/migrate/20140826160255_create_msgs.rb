class CreateMsgs < ActiveRecord::Migration
  def change
    create_table :msgs do |t|
      # 消息的类型, text, image
      t.string :msg_type, default: 'text'
      # 短信的文字内容/图片 URL
      t.string :message
      # 是否为直播短信
      t.boolean :is_live, default: false
      # 此消息是否为开启状态, 开启状态才可以公示出来
      t.boolean :is_open, default: false
      t.boolean :is_bless, default: false

      t.string :user_id
      t.string :user_name

      t.timestamps
    end
  end
end
