environment 'production'
bind 'tcp://0.0.0.0:8999'
pidfile './tmp/pids/puma.pid'
# 使用多进程, 但线程数量减少.让系统并发的解析文件
workers 2
threads 2, 5
preload_app!
